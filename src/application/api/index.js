import Http from '@/application/axios'
class HttpRequest {
     getLogin(param) {
        return Http.get('/login', param);
    }

    getBanner(param) {
        return Http.get('/banner', param);
    }

    getList(param) {
        return Http.get('/list', param);
    }

    getRegest(param) {
        return Http.get('/regest', param);
    }

    //招募的人
    getProUserList(param) {
        return Http.post('/card/api/organization-store/list', param);
    }
}

export  default  HttpRequest