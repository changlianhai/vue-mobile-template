import  Vue from 'vue'
import  VueRouter from 'vue-router'
Vue.use(VueRouter);

//引入页面组件
const Index = resolve => require(['../../pages/index'], resolve);
const Login = resolve => require(['../../pages/login'], resolve);
const Charts = resolve => require(['../../pages/charts'], resolve);
const List = resolve => require(['../../pages/list'], resolve);
const MyManage = resolve => require(['../../pages/myManage'], resolve);
const ShopDetails = resolve => require(['../../pages/shop_details'], resolve);

const routes = [
    {
        path: '/',
        name: 'index',
        component: Index,
        meta: {
            title: '首页'
        }
    },
    {
        path: '/index',
        name: 'index',
        meta: {
            title: '首页'
        },
        component: Index
    },
    {
        path: '/login',
        name: 'login',
        meta: {
            title: '登录'
        },
        component: Login
    },
    {
        path: '/charts',
        name: 'charts',
        meta: {
            title: '图表'
        },
        component: Charts
    },
    {
        path: '/list',
        name: 'list',
        meta: {
            title: '列表'
        },
        component: List
    },
    {
        path: '/myManage',
        name: 'myManage',
        meta: {
            title: '管理'
        },
        component: MyManage
    },
    {
        path: '/shop_details',
        name: 'shop_details',
        meta: {
            title: '商品详情'
        },
        component: ShopDetails
    },
];

const router = new VueRouter({
    routes,
    mode:'hash',
    scrollBehavior() {
        return {x:0, y:0}
    },
    linkExactActiveClass: 'my_active'
});

//跳转前拦截
router.beforeEach((to, from, next) => {
    //设置title
    if (to.meta.title) {
        document.title = to.meta.title;
    }
    next();
});

//跳转后拦截
router.afterEach(() => {

});
export default router;


