import Vue from 'vue'
import App from './App.vue'
import router from './application/router'
import store  from './application/vuex/store'

//调试模式
Vue.config.productionTip = false;

//移动端Rem单位适配
import  './assets/js/initRem'

//公用css
import './assets/css/reset.css'
import './assets/css/common.css'

//移动端点击延迟300ms解决
import fastclick from 'fastclick'
fastclick.attach(document.body);


//移动端vant组件库
import Vant from 'vant';
import 'vant/lib/index.css';


// vue pc端ui框架 -- iview
import iView from 'iview'
import 'view-design/dist/styles/iview.css';

// 兼容低版本Android
import 'babel-polyfill'
import Es6Promise from 'es6-promise'

require('es6-promise').polyfill();
Es6Promise.polyfill();

//vue全局过滤器
import  './assets/js/filter'

//所有请求API集合
import HttpRequest from './application/api'

//工具类函数
import Util from './assets/js/util'

//JS内置类原型上扩展方法(Object不可以)
import extendsFn from './assets/js/extends'
extendsFn();

//项目所有错误消息对应code集合
import errorCode from './assets/js/error_code'

//微信js-sdk， 开发微信公众号使用
import wx from 'weixin-js-sdk'

//插件集合
//复制文本到剪贴板
import VueClipboard from 'vue-clipboard2'

// 裁切图片
import VueCropper from 'vue-cropper'

//阿里视频播放
import aliplayer from 'vue-aliplayer'

//vue移动端调试工具 -- vconsole
import VConsole from 'vconsole'

const vConsole = new VConsole();
window.console.log(vConsole);

//移动端可视化插件
//https://v-charts.js.org/#/
import vChart from 'v-charts'

//基于Vue 2和高德地图的地图组件
//https://elemefe.github.io/vue-amap/#/zh-cn/introduction/install
import VueAMap from 'vue-amap';


//方法挂载到Vue类的原型上
Vue.prototype.$http = new HttpRequest;
Vue.prototype.$util = new Util;
Vue.prototype.$wx = wx;
Vue.prototype.$code = errorCode;


//处理项目用的OSS图片，微信JS-SDK分享
//开发环境
// devtest 开发环境   testing  生产环境
if (process.env.NODE_ENV === 'development') {
    //开发环境测试服务器接口
    if (process.env.VUE_APP_BUILD === 'devtest') {
        Vue.prototype.osspath   = 'http://longbei-test-media-out.oss-cn-beijing.aliyuncs.com/';
        Vue.prototype.shareUrl  = 'http://gwapi.longbei.com/pay/pay/auth/notify-url';
    } else { //开发环境本地服务器接口
        Vue.prototype.osspath   = 'http://longbei-dev-media-out.oss-cn-beijing.aliyuncs.com/';
        Vue.prototype.shareUrl  = 'http://gwapi.longbei.com/pay/pay/auth/notify-url';
    }
} else {
    //线上环境测试服务器接口
    if (process.env.VUE_APP_BUILD === 'testing') {
        Vue.prototype.osspath   = 'http://longbei-test-media-out.oss-cn-beijing.aliyuncs.com/';
        Vue.prototype.shareUrl  = 'http://wx-pay-test.longbei.com/pay/pay/auth/notify-url';
    } else { //线上环境生产服务器接口
        Vue.prototype.osspath   = 'https://longbei-pro-media-out.oss-cn-hangzhou.aliyuncs.com/';
        Vue.prototype.shareUrl  = 'http://gwapi.longbei.com/pay/pay/auth/notify-url';
    }
}
//注入插件
Vue.use(iView).use(VueClipboard).use(VueCropper).use(aliplayer).use(vChart).use(VueAMap).use(Vant);

window.vm = new Vue({
    router,
    store,
    render: h => h(App),
    created() {
        //调试
        //做缓存
        window.console.log(1111);
        window.console.dir(this.$http);
        window.console.dir(this.$util);
        window.console.dir(this.$wx);
        window.console.dir(this.$code);
    },
    mounted() {
        window.console.log(window.vm);
        window.console.log(1111);
    }
}).$mount('#app');

