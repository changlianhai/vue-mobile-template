class Util {
    /**
     * 判断字符串是否为手机号
     * @param {{string}}    str 字符串
     * 返回布尔值true/false
     */
    isMobile(str) {
        return /^1[3456789]\d{9}$/.test(str);
    }

    /**
     * 判断字符串是否为邮箱
     * @param {{string}}    str 字符串
     * 返回布尔值true/false
     */
    isEmail(str) {
        // return /^[a-zA-Z0-9]+([._-]*[a-zA-Z0-9]*)*@[a-zA-Z0-9]+.[a-zA-Z0-9{2,5}$]/.test(value);
        return str;
    }

    /**
     * 判断字符串是否身份证
     * @param {{string}}    card 字符串
     * 返回布尔值true/false
     */
    isIdCard(card) {
        let code = card.toUpperCase();
        let city = {
            11: "北京",
            12: "天津",
            13: "河北",
            14: "山西",
            15: "内蒙古",
            21: "辽宁",
            22: "吉林",
            23: "黑龙江 ",
            31: "上海",
            32: "江苏",
            33: "浙江",
            34: "安徽",
            35: "福建",
            36: "江西",
            37: "山东",
            41: "河南",
            42: "湖北 ",
            43: "湖南",
            44: "广东",
            45: "广西",
            46: "海南",
            50: "重庆",
            51: "四川",
            52: "贵州",
            53: "云南",
            54: "西藏 ",
            61: "陕西",
            62: "甘肃",
            63: "青海",
            64: "宁夏",
            65: "新疆",
            71: "台湾",
            81: "香港",
            82: "澳门",
            91: "国外 "
        };
        let pass = true;

        if (!code ||
            !/^[1-9][0-9]{5}(19[0-9]{2}|200[0-9]|2010)(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|3[01])[0-9]{3}[0-9xX]$/i
                .test(code)) {
            pass = false;
        } else if (!city[code.substr(0, 2)]) {
            pass = false;
        } else {
            //18位身份证需要验证最后一位校验位
            if (code && code.length == 18) {
                code = code.split('');
                //∑(ai×Wi)(mod 11)
                //加权因子
                let factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8,
                    4, 2
                ];
                //校验位
                let parity = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2];
                let sum = 0;
                let ai = 0;
                let wi = 0;
                for (let i = 0; i < 17; i++) {
                    ai = code[i];
                    wi = factor[i];
                    sum += ai * wi;
                }
                if (parity[sum % 11] != code[17]) {
                    pass = false;
                }
            }
        }
        return pass = pass ? true : false;
    }


    /**
     * 判断字符串是否银行卡号
     * 银行卡号有以下情况建行16、19，农行19，工行19、交通17、民生16、兴业18、招行12、16、19
     * @param {{string}}    card 字符串
     * 返回布尔值true/false
     */
    isBankCard(card) {
        return /^([1-9]{1})(\d{11}|\d{15}|\d{16}|\d{17}|\d{18})$/.test(card);
    }

    /**
     * 判断字符串是否为手机号
     * @param {{string}}    str 字符串
     * 返回布尔值true/false
     */
    checkMobileCode(n, val) {
        let reg = new RegExp("^\\d{"+ n +"}$" );
        return reg.test(val);
    }


    /**
     * 判断字符串是否为身份证
     * @param {{string}}    str 字符串
     * 返回布尔值true/false
     */
    isCard(str) {
        return /(^\d{15}$)|(^\d{18}$)|((^\d{17}(\d|X|x)$))/.test(str);
    }

    /**
     * 判断字符串是否为URL
     * @param {{string}}    str 字符串
     * 返回布尔值true/false
     */
    isUrl(str) {
        // return /^(http:|ftp:|https:)\/\/[A-Za-z0-9]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"])*$/.test(str);
        return str;
    }

    /**
     * 判断字符串是否为IPV4地址
     * @param {{string}}    str 字符串
     * 返回布尔值true/false
     */
    isIp(str) {
        return /^(0|[1-9]\d?|[0-1]\d{2}|2[0-4]\d|25[0-5]).(0|[1-9]\d?|[0-1]\d{2}|2[0-4]\d|25[0-5]).(0|[1-9]\d?|[0-1]\d{2}|2[0-4]\d|25[0-5]).(0|[1-9]\d?|[0-1]\d{2}|2[0-4]\d|25[0-5])$/.test(str);
    }

    /**
     * 判断只是汉字 字母 数字 下划线
     * @param {{string}}    str 字符串
     * 返回布尔值true/false
     */
    isNickname(str) {
        return /^[a-zA-Z0-9_\u4e00-\u9fa5]+$/.test(str);
    }

    /**
     * 判断是否在是微信环境运行
     * 返回布尔值true/false
     */
    isWexin() {
        let ua = window.navigator.userAgent.toLowerCase();
        return ua.match(/MicroMessenger/i) == 'micromessenger' ? true : false;
    }

    /**
     * 判断只是 字母 数字 下划线
     * @param {{string}}    str 字符串
     * 返回布尔值true/false
     */
    isAccount(str) {
        // return /^[a-zA-Z0-9_\.]+$/g.test(str);
        return str;

    }

    /**
     * 判断只是 字母 数字 ，多用于验证密码
     * @param {{string}}    str 字符串
     * 返回布尔值true/false
     */
    checkPwd(str) {
        return /^[a-zA-Z0-9]+$/g.test(str);
    }

    /**
     *获取页面Url的查询字符串的以对象形式返回
     * @param {{string}}    url 地址
     * @param {{string}}    key 要获取值得的ke'y
     */
    getQsObj(url, key) {
         url = (url || window.location.href);
        if (url.indexOf('?') === -1) return;
        url = decodeURIComponent(url); //解码
        let params = {};
        let urls = url.split("?");
        let arr = urls[1].split("&");
        for (let i = 0, l = arr.length; i < l; i++) {
            let a = arr[i].split("=");
            params[a[0]] = a[1];
        }
        return key ? params[key] : params;
    }


    /**
     *去除字符串中的HTML标签
     * @param {{string}} str 字符串
     */
    removeHTMLTag(str) {
        str = str + "";
        str = str.replace(/<\/?[^>]*>/g, ''); //去除HTML tag
        str = str.replace(/[ | ]*\n/g, '\n'); //去除行尾空白
        str = str.replace(/&nbsp;/ig, '');//去掉&nbsp;
        return str;
    }

    /**
     * 函数节流方法
     * @param {{Function}} fn 延时调用函数
     * @param {{Number}} delay 延迟多长时间
     * @param {{Number}} atleast 至少多长时间触发一次
     * @return {{Function}} 延迟执行的方法
     */
    throttle(fn, delay, atleast) {
        let timer = null;
        let previous = null;

        return function () {
            let now = +new Date();

            if (!previous) previous = now;

            if (now - previous > atleast) {
                fn();
                // 重置上一次开始时间为本次结束时间
                previous = now;
            } else {
                clearTimeout(timer);
                timer = setTimeout(function () {
                    fn();
                }, delay);
            }
        }
    }

    /**
     * 转移特殊符号
     * @param {{string}}    str 字符串
     * 返回转移后的字符串
     */
    unhtml(str) {
        if (str) {
            str = str.replace(/&lt;/g, '<');
            str = str.replace(/&gt;/g, '>');
            str = str.replace(/&quot;/g, '"');
            return str;
        }
        return '';
    }

    /**
     * 图片转为base64 1.图片onload完成，在进行canvas绘制，利用canvas转图片方法完成
     * @param {{String}} imgUrl 图片的URL地址
     * @param {{Function}}      callback 回调函数
     */
    getBase64Image(imgUrl, callback) {
        if(imgUrl) {
            const img = new Image();
            img.src = imgUrl;
            img.setAttribute('crossOrigin', 'anonymous'); //跨域解决
            img.onload = () => {
                let canvas = document.createElement("canvas");
                canvas.width = img.width;
                canvas.height = img.height;
                let ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0, img.width, img.height);
                let ext = img.src.substring(img.src.lastIndexOf(".")+1).toLowerCase();
                let base64Url = canvas.toDataURL("image/"+ext);
                callback && callback(base64Url)
            };
        }
        return '';
    }

    /**
     * 将base64图片url,formData
     * @param   {{String}}  dataurl 图片的base64
     * @returns {Blob} formData 对象
     */
    dataURLtoBlob(dataurl) {
        if(dataurl) {
                let arr = dataurl.split(','),
                mime = arr[0].match(/:(.*?);/)[1],
                bstr = atob(arr[1]),
                n = bstr.length,
                u8arr = new Uint8Array(n);
            while(n--){
                u8arr[n] = bstr.charCodeAt(n);
            }

            let file = new Blob([u8arr], { type: mime });
            let formData = new FormData();
            formData.append("contentType", 'image/jpeg');
            formData.append('prefixfile', 'avatar');
            formData.append("file", file);
            return formData;
        }
        return null;
    }


    /**
     * 生成随机数 min ~ max
     * @param min
     * @param max
     */
    randomBetween(min, max) {
        return  min + (Math.random() * (max-min +1));
    }

    /**
     * 检测数据类型, 返回对应的数据类型
     * @param val 要检测的数据
     * @returns {string} 返回对应的数据类型  Array, Number, String, Null, Undefined, Object ...
     */
    typeOf(val) {
        return Object.prototype.toString.call(val).slice(8, -1);
    }

    /**
     * 对象的深刻拷贝
     * @returns {[]|{}}
     */
    deepCopy(obj) {
        let result = Array.isArray(obj) ? [] : {};
        for(let key in obj) {
            if(obj.hasOwnProperty(key)) {
                if(Object.prototype.toString.call(obj[key]) === '[object Object]') {
                    result[key] = this.deepCopy( obj[key]);
                } else {
                    result[key] = obj[key];
                }
            }
        }
        return result;
    }

    /**
     * 微信公众号: 配置js-sdk
     * @param {{Object}}    _this   当前vue实例对象
     * @param {{String}}    url     页面地址
     * @return void;
     */
    setJSSDK(url) {
        window.vm.$http.getJSSDKconfig({
            url: window.encodeURIComponent(url),
        }).then(res => {
            if(res && res.data) {
                let data = res.data;
                let jsApiList = [
                    'chooseImage',
                    'startRecord',
                    'playVoice',
                    'stopRecord',
                    'downloadVoice',
                    'uploadVoice',
                    'stopVoice',
                    'translateVoice',
                    'chooseWXPay',
                    'onMenuShareTimeline',
                    'onMenuShareAppMessage',
                    'onMenuShareQQ',
                    'onMenuShareWeibo',
                    'onMenuShareQZone',
                    'scanQRCode',
                    'updateAppMessageShareData',
                    'hideMenuItems',
                    'updateTimelineShareData'
                ];
                window.vm.$wx.config({
                    debug: false,
                    appId: data.appid, //公众号的唯一标识
                    timestamp: data.timestamp, // 生成签名的时间戳
                    nonceStr: data.noncestr, // 生成签名的随机串
                    signature: data.signature, //签名
                    jsApiList
                });
                window.vm.$wx.ready(() => {
                    let shareUrl = 'http://gwapi.longbei.com/pay/pay/auth/notify-url'; //生产环境分享
                    //分享对象
                    let shareObj = {
                        title: '我已成为龙校爱心VIP，将与远方的小伙伴共同学习！共同成长！',
                        desc: '同在蓝天下|龙杯公益基金会联合爱心机构面向全国中小学生首发爱心VIP会员卡',
                        link: shareUrl,
                        imgUrl: 'http://longbei-test-media-out.oss-cn-beijing.aliyuncs.com//uipic/love_vip.png',
                        success: () => {
                            // console.log(res);
                        },
                        error: () => {
                            // console.log(err);
                        }
                    };
                    //分享
                    window.vm.$wx.updateAppMessageShareData(shareObj);
                    window.vm.$wx.updateTimelineShareData(shareObj);
                });
            }
        });
    }

    /**
     *  微信支付
     * @param  {{Object}}      _this      当前vue实例对象
     * @param  {{Object}}      data       配置JS-SDK数据
     * @return {{Function}}    success    支付成功
     * @return {{Function}}    fail       支付失败
     */
    wxPayment(data, success, fail) {
        if(data) {
            window.vm.$wx.chooseWXPay({
                timestamp: data.timeStamp,
                nonceStr: data.nonceStr,
                package: data.package,
                signType: data.signType,
                paySign: data.sign,
                success,
                fail
            });
        } else {
            // window.vm.$dialog.toast({mes:window.vm.$code[1]})
        }
    }
}
export default Util;