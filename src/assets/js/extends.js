
function extendsObject() {

    /**
     * 数组去重
     */
    Array.prototype.myUnique = function () {
        return [... new Set(this)];
    };

    /**
     * 数组取最大值
     */
    Array.prototype.getMax = function() {
      return Math.max(...this);
    };

    /**
     * 数组取最小值
     */
    Array.prototype.getMin = function() {
        return Math.min(...this);
    };

    /**
     * 删除指定元素
     * 改变原来的数组,返回this
     */
    Array.prototype.remove = function(val) {
        let index = this.indexOf(val);
        while(index>-1){
            this.splice(index, 1);
            index = this.indexOf(val);
        }
        return this;
    };
}

export  default  extendsObject;

