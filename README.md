# vue移动端模板
## vue-cli3
## 安装依赖
```
npm install
```

### 本地起服务(本地接口)
```
npm run serve
```

### 测试起服务(测试接口)
```
npm run devtest
```

### 线上打包
```
npm run build
```

### 测试打包
```
npm run testing
```


### Lints and fixes files
```
npm run lint
```

### 配置项目
See [Configuration Reference](https://cli.vuejs.org/config/).
```androiddatabinding
vue.config.js  文件配置
```

### vue移动端模板
1. vue基本
2. vue-router 路由
3. axios请求
4. js-sdk微信公众号开发
5. rem单位设置
6. 插件集合 视频，裁剪，复制，图表...
7. 常用方法封装
8. 项目统一错误码集合
9. 项目分本地环境，测试环境，线上环境
10.支持less
11.vuex状态管理
12.public 静态资源，静态页面存储地方，图片 
